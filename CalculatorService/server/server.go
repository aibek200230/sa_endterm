package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {
	l, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	log.Println("Server is running on port:9000")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}

}
